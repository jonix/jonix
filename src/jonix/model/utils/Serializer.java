package jonix.model.utils;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializer {

	public static String writeXML(Object f) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLEncoder encoder = new XMLEncoder(baos);
		encoder.writeObject(f);
		encoder.close();
		return baos.toString();
	}

	public static Object readXML(String buffer) {
		XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(
				buffer.getBytes()));
		Object o = (Object) decoder.readObject();
		decoder.close();
		return o;
	}

	public static Object readBytes(byte[] buffer) {
		ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(bais);
			Object obj = ois.readObject();
			ois.close();
			return obj;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] writeBytes(Object f) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(f);
			oos.flush();
			oos.close();
			return baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
