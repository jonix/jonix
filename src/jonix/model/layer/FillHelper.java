package jonix.model.layer;

import jonix.model.IModelContext;

public class FillHelper extends Layer {

	private static final long serialVersionUID = 1L;

	public static final String FILL_HELPER = "Fill helper";

	public FillHelper(IModelContext context) {
		super(context, FILL_HELPER);
	}

}
