package jonix.model.layer;

import jonix.model.IModelContext;

public class Path extends Layer {

	private static final long serialVersionUID = 1L;

	public final static String PATH = "Path";

	private boolean isActive = false;

	public Path(IModelContext context) {
		super(context, PATH);
	}

	public boolean isActive() {
		return isActive;
	}

	public void activate() {
		isActive = true;
	}

	public void deactivate() {
		isActive = false;
	}

}
