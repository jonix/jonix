package jonix.model.layer;

import jonix.model.IModelContext;

public class Earth extends Layer {

	private static final long serialVersionUID = 1L;

	public final static String EARTH = "Earth";

	private double areaValue;

	public Earth(IModelContext context) {
		super(context, EARTH);
	}

	public double areaValue() {
		return areaValue;
	}

	public void updateAreaValue() {
		int counter = 0;
		int[][] cells = getCells();
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[i].length; j++) {
				if (cells[i][j] != EMPTY)
					counter++;
			}
		}
		areaValue = ((double) counter)
				/ ((double) (getContext().getWidth() * getContext().getHeight()));
	}

}
