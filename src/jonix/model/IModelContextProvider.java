package jonix.model;

public interface IModelContextProvider {

	public IModelContext getContext();

}
