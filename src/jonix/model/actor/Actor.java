package jonix.model.actor;

import jonix.model.AbstractModel;
import jonix.model.IModelContext;

public class Actor extends AbstractModel {

	private static final long serialVersionUID = 1L;

	private int x;

	private int y;

	private int width;

	private int height;

	private int speedX = 0;

	private int speedY = 0;

	public Actor(IModelContext context, String name, int x, int y, int width,
			int height, int speedX, int speedY) {
		super(context, name);
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.speedX = speedX;
		this.speedY = speedY;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public void setSpeed(int x, int y) {
		setSpeedX(x);
		setSpeedY(y);
	}

	public void stop() {
		setSpeed(0, 0);
	}

}
