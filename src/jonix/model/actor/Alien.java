package jonix.model.actor;

import jonix.model.IModelContext;

public class Alien extends Actor {

	private static final long serialVersionUID = 1L;

	public Alien(IModelContext context, String name, int x, int y, int width,
			int height, int speedX, int speedY) {
		super(context, name, x, y, width, height, speedX, speedY);
	}

}
