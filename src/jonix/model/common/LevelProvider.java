package jonix.model.common;

import java.util.HashMap;
import java.util.Map;

import jonix.model.IModelContext;
import jonix.model.IModelContextProvider;

public abstract class LevelProvider implements IModelContextProvider {

	private Map<Integer, Level> levels;

	private IModelContext context;

	public LevelProvider(IModelContext context) {
		this.context = context;
	}

	public Level getLevel(int level) {
		return getLevels().get(level);
	}

	public int getLevelCount() {
		return getLevels().size();
	}
	
	private Map<Integer, Level> getLevels() {
		if (levels == null) {
			levels = new HashMap<Integer, Level>();
			initializeLevels();
		}
		return levels;
	}

	protected void addLevel(Level level) {
		levels.put(levels.size(), level);
	}

	@Override
	public IModelContext getContext() {
		return context;
	}

	abstract protected void initializeLevels();

}
