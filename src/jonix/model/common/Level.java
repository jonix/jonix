package jonix.model.common;

import jonix.model.World;

public abstract class Level {

	abstract public void initializeWorld(World world);
	
	abstract public boolean isFinished(World world);

}
