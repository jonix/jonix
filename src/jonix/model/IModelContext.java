package jonix.model;

public interface IModelContext {

	public int getWidth();

	public int getHeight();

}
