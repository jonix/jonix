package jonix.model;

import java.io.Serializable;
import java.util.List;

public interface IModel extends Serializable, IModelContextProvider {

	public void setParent(IModel abstractModel);

	public IModel getParent();

	public void addChild(IModel model);

	public void removeChild(IModel model);

	public IModelContext getContext();

	public void down();

	public void up();

	public void left();

	public void right();

	public String getName();

	public IModel getModel(String name);

	public List<IModel> getChilds();

	public void calculate();

	public void apply();

	public void setContext(IModelContext context);
	
	public void removeChilds();

}
