package jonix.model.t1;

import jonix.model.IModelContext;
import jonix.model.World;
import jonix.model.actor.Alien;
import jonix.model.actor.Player;
import jonix.model.common.Level;
import jonix.model.common.LevelProvider;
import jonix.model.layer.Earth;
import jonix.model.layer.FillHelper;
import jonix.model.layer.Path;

public class T1LevelProvider extends LevelProvider {

	private int tileSize = 10;

	public T1LevelProvider(IModelContext context) {
		super(context);
	}

	private void configureCommonParts(World world) {
		IModelContext context = getContext();
		Earth earth = new Earth(context);
		Path path = new Path(context);
		earth.fillMarker(0, 0, tileSize, context.getHeight());
		earth.fillMarker(context.getWidth() - tileSize, 0, tileSize,
				context.getHeight());
		earth.fillMarker(0, 0, context.getWidth(), tileSize);
		earth.fillMarker(0, context.getHeight() - tileSize, context.getWidth(),
				tileSize);

		world.addChild(path);
		world.addChild(earth);
		world.addChild(new FillHelper(context));
		world.addChild(new Player(context, 0, 0, tileSize, tileSize, 0, 0));
	}

	@Override
	protected void initializeLevels() {
		addLevel(new Level() {
			@Override
			public void initializeWorld(World world) {
				configureCommonParts(world);
				world.addChild(new Alien(getContext(), "Alien 1", 150, 150,
						tileSize, tileSize, 1, 1));
			}

			@Override
			public boolean isFinished(World world) {
				return world.getEarth().areaValue() > 0.6;
			}
		});

		addLevel(new Level() {
			@Override
			public void initializeWorld(World world) {
				configureCommonParts(world);
				world.addChild(new Alien(getContext(), "Alien 1", 100, 100,
						tileSize, tileSize, 1, 1));
				world.addChild(new Alien(getContext(), "Alien 2", 100, 150,
						tileSize, tileSize, -1, -1));
			}

			@Override
			public boolean isFinished(World world) {
				return world.getEarth().areaValue() > 0.7;
			}
		});

		addLevel(new Level() {
			@Override
			public void initializeWorld(World world) {
				configureCommonParts(world);
				world.addChild(new Alien(getContext(), "Alien 1", 100, 100,
						tileSize, tileSize, 1, 1));
				world.addChild(new Alien(getContext(), "Alien 2", 100, 150,
						tileSize, tileSize, -2, -2));
				world.addChild(new Alien(getContext(), "Alien 3", 30, 30,
						tileSize, tileSize, -1, -1));
			}

			@Override
			public boolean isFinished(World world) {
				return world.getEarth().areaValue() > 0.75;
			}
		});

		addLevel(new Level() {
			@Override
			public void initializeWorld(World world) {
				configureCommonParts(world);
				world.addChild(new Alien(getContext(), "Alien 1", 100, 100,
						tileSize, tileSize, 1, 1));
				world.addChild(new Alien(getContext(), "Alien 2", 100, 150,
						tileSize, tileSize, -1, -1));
				world.addChild(new Alien(getContext(), "Alien 3", 30, 30,
						tileSize, tileSize, -2, -2));
				world.addChild(new Alien(getContext(), "Alien 4", 120, 220,
						tileSize, tileSize, 1, 1));
			}

			@Override
			public boolean isFinished(World world) {
				return world.getEarth().areaValue() > 0.8;
			}
		});

		addLevel(new Level() {
			@Override
			public void initializeWorld(World world) {
				configureCommonParts(world);
				world.addChild(new Alien(getContext(), "Alien 1", 30, 60,
						tileSize, tileSize, 1, 1));
				world.addChild(new Alien(getContext(), "Alien 2", 130, 60,
						tileSize, tileSize, -2, -2));
				world.addChild(new Alien(getContext(), "Alien 3", 100, 100,
						tileSize, tileSize, 2, 2));
				world.addChild(new Alien(getContext(), "Alien 4", 100, 150,
						tileSize, tileSize, -1, -1));
				world.addChild(new Alien(getContext(), "Alien 5", 100, 250,
						tileSize, tileSize, -1, -1));
			}

			@Override
			public boolean isFinished(World world) {
				return world.getEarth().areaValue() > 0.9;
			}
		});

	}

}
