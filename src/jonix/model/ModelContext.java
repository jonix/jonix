package jonix.model;

public class ModelContext implements IModelContext {

	private int width;

	private int height;

	public ModelContext(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

}
