package jonix.test;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class AppFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private AppPanel panel;

	private Config config;

	private Thread thread;

	private boolean isInitialized = false;

	private boolean threadRunned = true;

	public AppFrame(Config config) {
		this.config = config;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addComponentsToPane();
		setSize(config.getWidth(), config.getHeight());
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// FIXME: should be moved to center
		setLocation(screenSize.width - config.getWidth(), screenSize.height - config.getHeight());
		/*setLocation((screenSize.width - config.getWidth()) / 2,
				(screenSize.height - config.getHeight()) / 2);*/
		setVisible(true);

		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar() == 's') {
					getPanel().sendKey('s');
				} else if(e.getKeyChar() == 'l') {
					getPanel().sendKey('l');
				} else if(e.getKeyChar() == 'p') {
					getPanel().sendKey('p');
				} else if(e.getKeyChar() == 'c') {
					getPanel().sendKey('c');
				} else if(e.getKeyChar() == 'r') {
					getPanel().sendKey('r');
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				getPanel().sendKey(e.getKeyCode());
				repaint();
			}
		});

		thread = new Thread() {

			@Override
			public void run() {
				while (threadRunned) {
					try {
						sleep(10);
						AppFrame.this.tick();
						repaint();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		};

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				threadRunned = false;
				try {
					thread.join();
				} catch (InterruptedException e1) {
					//e1.printStackTrace();
				}
				System.exit(0);
			}
		});
	}

	protected void tick() {
		getPanel().tick();
	}

	protected void addComponentsToPane() {
		addComponentsToContentPane(getContentPane());
	}

	protected void addComponentsToContentPane(Container container) {
		container.add(getPanel(), BorderLayout.CENTER);
	}

	protected AppPanel getPanel() {
		if (panel == null) {
			panel = new AppPanel(config);
		}
		return panel;
	}

	@Override
	public void paint(Graphics g) {
		if (!isInitialized) {
			isInitialized = true;
			thread.start();
		}
		super.paint(g);
	}

}
